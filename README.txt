DROOP
Installation Profile

Developed by Aaron Winborn
http://www.advomatic.com/
winborn (at) advomatic (dot) com

NOTE: These are notes to get the project started. Initial development is planned for late March 2007.
Feel free to contact me for feedback or other comments.

This profile will be a Scoop killer...

TODO:
Everything

Modules:
cck
views
contemplate
karma
imagefield
imagecache
video_cck
subscribe
send
bookmark_us
role_delay
troll
recommended_diary
service_links
tagadelic

Roles:
admin -- full administration access
editor -- able to enter the various types
contributor

Content Types:
diary (created in cck/views) with teaser entry (intro/body)
quick links (300-400 characters in a quick link format, like right col of mydd.com)
blogroll (links to blogs)
blog (same as diary, but for contributors)
